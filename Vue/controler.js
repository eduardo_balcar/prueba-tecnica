const app = new Vue({
    el: '#app',
    data:{
        titulo: 'Prueba tecnica',
        products: [
            'uno',
            'dos',
            'tres'
        ],
        codigo: '',
        contract: {},
        btnDisabled: 0
    },
    methods:{
        callApi(){
            this.btnDisabled = 1;
            fetch('https://localhost:7051/api/contract/detail/' + this.codigo)
        .then(response => response.json())
        .then(json => {
            console.log(json)
            if(json.data == undefined) window.alert(json.error.message)
            else this.contract = json.data
            //console.log(this.contract.contractItems)
        })
        .catch(error => window.alert(error))
        this.btnDisabled = 0
        },
        formatoFecha(fecha) {
            const fechaObj = new Date(fecha);
            const dia = fechaObj.getDate().toString().padStart(2, '0');
            const mes = (fechaObj.getMonth() + 1).toString().padStart(2, '0');
            const anio = fechaObj.getFullYear().toString();
            if(dia == 'NaN') return ''
            return `${dia}/${mes}/${anio}`;
          }
    }
    // created(){
    //     fetch('https://localhost:7051/api/contract/detail/' + 105)
    //     .then(response => response.json())
    //     .then(json => {
    //         console.log(json)
    //         this.contract = json.item
    //         console.log(contract)
    //     })
    // }
});