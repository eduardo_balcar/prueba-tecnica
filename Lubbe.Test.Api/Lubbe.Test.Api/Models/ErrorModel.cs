﻿namespace Lubbe.Test.Api.Models
{
    public class ErrorModel
    {
        public int Code { get; set; }
        public string Message { get; set; }

        public static ErrorModel NoError()
        {
            return new ErrorModel { Code = 0, Message = "" };
        }
    }
}
