﻿using Lubbe.Test.Api.Models.Data;

namespace Lubbe.Test.Api.Models.Dto
{
    public class ContractItemsDto
    {
        public ItemDto? Item { get; set; }
    }
}
