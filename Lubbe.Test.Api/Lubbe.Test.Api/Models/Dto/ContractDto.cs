﻿using Lubbe.Test.Api.Models.Data;

namespace Lubbe.Test.Api.Models.Dto
{
    public class ContractDto
    {
        public string? CoursesCode { get; set; }
        public DateTime FechaAlta { get; set; }
        public int CantidadEgresados { get; set; }
        public DateTime FechaEntrega { get; set; }
        public string? ColegioNivel { get; set; }
        public string? ColegioCurso { get; set; }
        public string? ColegioLocalidad { get; set; }
        public decimal Total { get; set; }
        public string? ColegioNombre { get; set; }
        public List<ContractItemsDto>? ContractItems { get; set; }
    }
}
