﻿namespace Lubbe.Test.Api.Models.Dto
{
    public class ItemDto
    {
        public string? Nombre { get; set; }
        public decimal Precio { get; set; }
    }
}
