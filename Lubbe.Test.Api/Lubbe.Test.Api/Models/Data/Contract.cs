﻿namespace Lubbe.Test.Api.Models.Data
{
    public class Contract
    {
        public int Id { get; set; }
        public string? CoursesCode { get; set; }
        public DateTime FechaAlta { get; set; }
        public byte Estado { get; set; }
        public int CantidadEgresados { get; set; }
        public DateTime FechaEntrega { get; set; }
        public string? MedioEntrega { get; set; }
        public string? Vendedor { get; set; }
        public string? ColegioNivel { get; set; }
        public string? ColegioCurso { get; set; }
        public string? ColegioLocalidad { get; set; }
        public string? Comision { get; set; }
        public decimal Total { get; set; }
        public string? ColegioNombre { get; set; }
        public virtual List<ContractItems>? ContractItems { get; set; }
    }
}
