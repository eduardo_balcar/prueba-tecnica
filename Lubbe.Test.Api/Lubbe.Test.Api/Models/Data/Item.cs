﻿namespace Lubbe.Test.Api.Models.Data
{
    public class Item
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public decimal Precio { get; set; }
    }
}
