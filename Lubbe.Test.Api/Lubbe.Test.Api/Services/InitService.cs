﻿namespace Lubbe.Test.Api.Services
{
    public class InitService
    {
        public Models.ErrorModel? LastError;

        public void ClearErrors()
        {
            LastError = null;
        }

        public void SetError(int code, string message)
        {
            LastError = new Models.ErrorModel();
            LastError.Code = code;
            LastError.Message = message;
            //LogHelper.LogError($"ERRORCODE: {code} MESSAGE: {message}"); Para hacer logue de errores
        }

        public bool HasErrors()
        {
            return (LastError != null);
        }
    }
}
