﻿

using Lubbe.Test.Api.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace Lubbe.Test.Api.Services
{
    public class ContractService: InitService
    {
        public Contract? GetContractById(string codigo)
        {
            ClearErrors();
            try
            {
                using (var context = new Data.DataContext())
                {

                    Contract? contract = null;
                    if (int.TryParse(codigo, out int id))
                        contract = context.Contract.Where(c => c.Id == id).FirstOrDefault();

                    else
                        contract = context.Contract.Where(c => c.CoursesCode.Equals(codigo.ToUpper())).FirstOrDefault();

                    if (contract == null)
                    {
                        SetError(404, $"No se encontraron elementos con ese codigo: {codigo}");
                        return null;
                    }

                    var contractItem = context.ContractItems.Include("Item").Where(ci => ci.ContractId == contract.Id).ToList();
                    contract.ContractItems = contractItem;

                    return contract;
                }
            }
            catch (Exception ex)
            {
                SetError(500, $"Error inesperado: {ex.Message} {ex.InnerException?.Message}");
                return null;
            }
           
        }
    }
}
