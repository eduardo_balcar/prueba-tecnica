﻿using Lubbe.Test.Api.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace Lubbe.Test.Api.Data
{
    public class DataContext : DbContext
    {
        public DataContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string cnstring = ConfigurationManager.AppSetting["ConnectionStringMysql"] ?? "";
            optionsBuilder.UseMySql(cnstring, ServerVersion.AutoDetect(cnstring));
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Contract> Contract { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ContractItems> ContractItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>().Property(i => i.Id).HasColumnName("id");
            modelBuilder.Entity<Item>().Property(i => i.Nombre).HasColumnName("nombre");
            modelBuilder.Entity<Item>().Property(i => i.Precio).HasColumnName("precio");
            modelBuilder.Entity<Item>().ToTable("items");

            modelBuilder.Entity<Contract>().Property(c => c.Id).HasColumnName("id");
            modelBuilder.Entity<Contract>().Property(c => c.CoursesCode).HasColumnName("courses_code");
            modelBuilder.Entity<Contract>().Property(c => c.FechaAlta).HasColumnName("fecha_alta");
            modelBuilder.Entity<Contract>().Property(c => c.Estado).HasColumnName("estado");
            modelBuilder.Entity<Contract>().Property(c => c.CantidadEgresados).HasColumnName("cantidad_egresados");
            modelBuilder.Entity<Contract>().Property(c => c.FechaEntrega).HasColumnName("fecha_entrega");
            modelBuilder.Entity<Contract>().Property(c => c.MedioEntrega).HasColumnName("medio_entrega");
            modelBuilder.Entity<Contract>().Property(c => c.Vendedor).HasColumnName("vendedor");
            modelBuilder.Entity<Contract>().Property(c => c.ColegioNivel).HasColumnName("colegio_nivel");
            modelBuilder.Entity<Contract>().Property(c => c.ColegioCurso).HasColumnName("colegio_curso");
            modelBuilder.Entity<Contract>().Property(c => c.ColegioLocalidad).HasColumnName("colegio_localidad");
            modelBuilder.Entity<Contract>().Property(c => c.Comision).HasColumnName("comision");
            modelBuilder.Entity<Contract>().Property(c => c.Total).HasColumnName("total");
            modelBuilder.Entity<Contract>().Property(c => c.ColegioNombre).HasColumnName("colegio_nombre");
            modelBuilder.Entity<Contract>().ToTable("contracts");

            modelBuilder.Entity<ContractItems>().Property(ci => ci.Id).HasColumnName("id");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.ContractId).HasColumnName("contract_id");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.ItemId).HasColumnName("item_id");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.CreateDate).HasColumnName("create_date");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.UpdateDate).HasColumnName("update_date");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.Enabled).HasColumnName("enabled");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.Deleted).HasColumnName("deleted");
            modelBuilder.Entity<ContractItems>().Property(ci => ci.CreatedBy).HasColumnName("created_by");
            modelBuilder.Entity<ContractItems>().ToTable("contract_items");
        }
    }
}
