﻿using Lubbe.Test.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Lubbe.Test.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractController : InitContoller
    {
        [HttpGet("detail/{codigo}")]
        public IActionResult GetCategoryById(string codigo)
        {
            var servicio = new ContractService();
            try
            {
                var contract = servicio.GetContractById(codigo);
                if (servicio.HasErrors())
                {
                    return Ok(new { error = servicio.LastError });
                }

                return Ok(new { data = Mappers.ModelMappers.MapContractToDto(contract) });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
