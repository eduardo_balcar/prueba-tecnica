﻿using Lubbe.Test.Api.Models.Data;
using Lubbe.Test.Api.Models.Dto;

namespace Lubbe.Test.Api.Mappers
{
    public class ModelMappers
    {
        public static ContractDto MapContractToDto (Contract contract) 
        {
            return new ContractDto()
            {
                FechaAlta = contract.FechaAlta,
                CantidadEgresados = contract.CantidadEgresados,
                ColegioCurso = contract.ColegioCurso,
                ColegioLocalidad = contract.ColegioLocalidad,
                ColegioNivel = contract.ColegioNivel,
                ColegioNombre = contract.ColegioNombre,
                CoursesCode = contract.CoursesCode,
                FechaEntrega = contract.FechaEntrega,
                Total = contract.Total,
                ContractItems = MapListContractItemsToDto(contract.ContractItems)
            };
        }

        public static ItemDto MapItemToDto(Item item)
        {
            return new ItemDto() { Nombre = item.Nombre, Precio = item.Precio };
        }

        public static ContractItemsDto MapContractItemsToDto (ContractItems contractItems) 
        {
            return new ContractItemsDto() { Item = MapItemToDto(contractItems.Item) };
        }

        public static List<ContractItemsDto> MapListContractItemsToDto (List<ContractItems> contractItems)
        {
            var list = new List<ContractItemsDto>();
            foreach (ContractItems item in contractItems)
            {
                var contractItem = MapContractItemsToDto(item);
                list.Add(contractItem);
            }
            return list;
        }
    }
}
