import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractListItemComponent } from './component/contract-list-item/contract-list-item.component';

const routes: Routes = [
  {path:'', component:ContractListItemComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
