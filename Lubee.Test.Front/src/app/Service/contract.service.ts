import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContractI } from '../Models/contract.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private http:HttpClient) { }

  private urlBase = environment.urlBase;
  
  public getContract(codigo:string):Observable<{data:ContractI, error:string}>{
    return this.http.get<{data:ContractI, error:string}>(this.urlBase + '/api/contract/detail/' + codigo)
  }

}
