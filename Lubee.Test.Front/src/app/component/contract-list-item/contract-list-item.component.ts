import { Component, OnInit } from '@angular/core';
import { ContractI } from 'src/app/Models/contract.interface';
import { ContractService } from 'src/app/Service/contract.service';

@Component({
  selector: 'app-contract-list-item',
  templateUrl: './contract-list-item.component.html',
  styleUrls: ['./contract-list-item.component.css']
})
export class ContractListItemComponent implements OnInit {

  constructor(private contractService:ContractService) { }

  contract: ContractI = {
    id: 0,
    coursesCode: '',
    fechaAlta: undefined,
    estado: false,
    cantidadEgresados: 0,
    fechaEntrega: undefined,
    medioEntrega: '',
    Vendedor: '',
    colegioNivel: '',
    colegioCurso: '',
    colegioLocalidad: '',
    comision: '',
    colegioNombre: '',
    total: 0,
    contractItems: []
  };
  codigo: string = '';
  btnDisabled = false;


  ngOnInit(): void {
  }

  ObtenerDetalle(codigo:string){
    this.btnDisabled = true;
    this.contractService.getContract(codigo).subscribe(res =>{
      if(res.data == null) window.alert(`El codigo ${codigo} no tiene informacion relacionada`)
      else this.contract = res.data
      if(res.error != null) window.alert(res.error)
      console.log(this.contract)
      this.btnDisabled = false;
    }, error =>{
      window.alert(error)
      this.btnDisabled = false;
    })
  }

}