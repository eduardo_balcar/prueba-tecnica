import { ItemI } from "./item.interface";

export interface ContractItemI {
    id: number, 
    contractId: number, 
    itemId:number, 
    createDate?: Date, 
    updateDate?: Date, 
    enabled: boolean, 
    deleted: boolean,
    createdBy?: string, 
    item: ItemI,
} 