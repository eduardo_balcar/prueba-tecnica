import { ContractItemI } from "./contract.item.interface";

export interface ContractI {
    id: number,
    coursesCode: string,
    fechaAlta?: Date,
    estado: boolean,
    cantidadEgresados: number,
    fechaEntrega?: Date,
    medioEntrega: string,
    Vendedor: string,
    colegioNivel: string,
    colegioCurso: string,
    colegioLocalidad: string,
    comision: string,
    total: number,
    colegioNombre: string,
    contractItems: ContractItemI[]
}