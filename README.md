# prueba-tecnica

Se solicita desarrollar app en vue.js que disponga de un formulario donde a partir de un id
ingresado muestre de forma readonly con la siguiente información de un contrato:

    ● "Código de curso: " + "Código de curso"
    ● "Fecha de alta: " + "Fecha de alta"
    ● "Colegio: "+ Colegio - Nombre
    ● "Nivel: " + Colegio - Nivel
    ● "Curso: " + Colegio - Curso
    ● "Localidad:" + Colegio - Localidad
    ● "Pedido: " (tantas filas como artículos asociados al contrato):
        o Columna "Cantidad" (Cantidad de egresados)
        o Columna "Artículo" (Artículo)
        o Columna "Precio unitario" (Precio)
        o Columna "Total" (Importe)
    ● "Total: " + Monto total a pagar
    ● "Fecha de entrega: " + Fecha de entrega

Para esto debe desarrollarse una API en .net que devuelva mediante un endpoint la
información necesaria tomada desde una base mysql con las siguientes tablas...

* La prueba se realizon utilizando una API .NET 6.
* Para la base de datos se utilizo una base de pureba de db4free.net en mysql. Los datos de acceso en
  appsettings.json de la API.
* Para el front se utilizo Angular pero tambien se hizo una app en Vue.js.

* id de pureba = 105 - 106
* codigo pureba = TRB123 - TRB124